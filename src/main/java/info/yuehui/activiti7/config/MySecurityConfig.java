// package info.yuehui.activiti7.config;
//
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
// import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
// import org.springframework.security.config.annotation.web.builders.HttpSecurity;
// import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
// import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
// import org.springframework.security.core.authority.SimpleGrantedAuthority;
// import org.springframework.security.core.userdetails.User;
// import org.springframework.security.core.userdetails.UserDetailsService;
// import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
// import org.springframework.security.crypto.password.PasswordEncoder;
// import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//
// import java.util.Arrays;
// import java.util.List;
// import java.util.stream.Collectors;
//
// @Configuration
// @EnableWebSecurity
// public class MySecurityConfig extends WebSecurityConfigurerAdapter {
//
//     @Override
//     @Autowired
//     public void configure(AuthenticationManagerBuilder auth) throws Exception {
//         auth.userDetailsService(myUserDetailsService()).passwordEncoder(passwordEncoder());
//     }
//
//     @Bean("userDetailsService")
//     public UserDetailsService myUserDetailsService() {
//         InMemoryUserDetailsManager inMemoryUserDetailsManager = new InMemoryUserDetailsManager();
//
//         String[][] usersGroupsAndRoles = {
//                 {"user", "123456", "ROLE_ACTIVITI_USER", "GROUP_TEST"},
//                 {"admin", "123456", "ROLE_ACTIVITI_ADMIN"}
//         };
//
//         for (String[] user : usersGroupsAndRoles) {
//             List<String> authoritiesStrings = Arrays.asList(Arrays.copyOfRange(user, 2, user.length));
//             inMemoryUserDetailsManager.createUser(new User(user[0], passwordEncoder().encode(user[1]),
//                     authoritiesStrings.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList())));
//         }
//
//         return inMemoryUserDetailsManager;
//     }
//
//     @Override
//     protected void configure(HttpSecurity http) throws Exception {
//         http.csrf().disable().authorizeRequests().anyRequest().authenticated().and().httpBasic();
//     }
//
//     @Bean
//     public PasswordEncoder passwordEncoder() {
//         return new BCryptPasswordEncoder();
//     }
// }