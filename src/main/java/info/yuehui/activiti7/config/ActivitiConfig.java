package info.yuehui.activiti7.config;

import org.activiti.api.runtime.shared.identity.UserGroupManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 覆盖activiti默认集成spring-security中的用户体系
 * @author zhangxing
 * @version v1.0
 * @date 2022/7/19 13:51
 */
@Configuration
public class ActivitiConfig {

    @Bean
    public UserGroupManager userGroupManager() {
        return new UserGroupManager(){
            @Override
            public List<String> getUserGroups(String s) {
                return null;
            }

            @Override
            public List<String> getUserRoles(String s) {
                return null;
            }

            @Override
            public List<String> getGroups() {
                return null;
            }

            @Override
            public List<String> getUsers() {
                return null;
            }
        };
    }

}
