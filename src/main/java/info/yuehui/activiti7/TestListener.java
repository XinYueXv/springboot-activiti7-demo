package info.yuehui.activiti7;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author zhangxing
 * @version v1.0
 * @date 2022/12/30 3:35 PM
 */
@Component
public class TestListener implements ExecutionListener, ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void notify(DelegateExecution delegateExecution) {
        System.out.println(applicationContext);
        System.out.println("执行了监听器1");
        int a = 1/0;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
