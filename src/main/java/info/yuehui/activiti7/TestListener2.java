package info.yuehui.activiti7;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

/**
 * @author zhangxing
 * @version v1.0
 * @date 2022/12/30 3:35 PM
 */
public class TestListener2 implements ExecutionListener {

    @Override
    public void notify(DelegateExecution delegateExecution) {
        System.out.println("执行了监听器2");
    }
}
