package info.yuehui.activiti7;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@SpringBootTest
class ApplicationTests {

    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private RepositoryService repositoryService;

    @Test
    public void start() {
        String defKey = "Process_1673500828052";
        log.info("开启请假流程...");
        Map<String, Object> map = new HashMap<>();
        map.put("abc", "123");
        //在holiday.bpmn中,填写请假单的任务办理人为动态传入的userId,此处模拟一个id
//        map.put("userId", "u0001");
//        map.put("roleId", "r0001,r0002");
        ProcessInstance instance = runtimeService.startProcessInstanceByKey(defKey, map);
        log.info("启动流程实例成功:{}", instance);
        log.info("流程实例ID:{}", instance.getId());
        log.info("流程定义ID:{}", instance.getProcessDefinitionId());
    }

    @Test
    public void processInstanceList() {
        List<ProcessInstance> list = runtimeService.createProcessInstanceQuery()
                .active().list();
        log.info("流程实例：{}", list);
    }

    @Test
    public void taskListTest() {
//        taskService.createTaskQuery()
        List<Task> list = taskService.createTaskQuery()
                // .processInstanceId("1d954b55-8ffb-11ed-97ca-b42e99ea65c9")
                .taskCandidateOrAssigned("zx", Arrays.asList("1", "2"))
                .active().list();
        log.info("任务：{}", list);
    }

    @Test
    public void taskCompleteTest() {
        // 转让任务/给任务设置经办人
        // taskService.claim(taskId, userId);
        Map<String, Object> map = new HashMap<>();
        map.put("isPass", "true");
        Authentication.setAuthenticatedUserId("1");
        taskService.complete("1d97e369-8ffb-11ed-97ca-b42e99ea65c9", map);
    }

    @Test
    public void claimTest() {
        String taskId = "1bb88698-074c-11ed-8a4f-b291312d3856";
        String userId = "10002";

        // 领取任务，当任务属于候选人或者候选组的时候，由满足条件的候选人来认领这个任务，认领成功任务才属于认领人
        taskService.claim(taskId, userId);
    }

    @Test
    public void setAssignerTest() {
        // 给任务修改经办人，直接改，不会留下任务历史记录
        String taskId = "1bb88698-074c-11ed-8a4f-b291312d3856";
        String userId = "10003";

        // 如果将当前任务转给其他人，可以同时设置任务本来属于谁，可以设置owner保存原始的经办人
        // taskService.setOwner(taskId, userId);
        taskService.setAssignee(taskId, userId);
    }

    @Test
    public void delegateTaskTest() {
        // 任务委托，将当前任务完成，然后开启一个新任务，交给委托人
        String taskId = "1bb88698-074c-11ed-8a4f-b291312d3856";
        String userId = "10004";
        taskService.delegateTask(taskId, userId);
    }

    /**
     * 只能导入文件名称为 [*.bpmn20.xml] 或者 [*.bpmn]
     */
    @Test
    public void deployTest() throws Exception {
        DeploymentBuilder builder = repositoryService.createDeployment();

        // 只能导入文件名称为 [*.bpmn20.xml] 或者 [*.bpmn]
        File file = new File("/Users/macos/Desktop/test.bpmn20.xml");
        Deployment deployment = builder
                .name("test_process_2222222")
                .key("key-test-sssssss")
                .addInputStream(file.getAbsolutePath(), new FileInputStream(file))
                .deploy();
        log.info("流程部署成功!");
        log.info("流程部署ID={}", deployment.getId());
        log.info("流程部署名称={}", deployment.getName());
    }

    @Test
    public void deleteProcessInstanceTest(){
        runtimeService.deleteProcessInstance("c6462135-881c-11ed-a763-b42e99ea65c9", "删除原因");
    }
    
    @Test
    public void processModelLTest(){
//        repositoryService.createModelQuery()
//                .modelCategory("入库单")
    }

    @Test
    public void Test() {
        System.out.println(1);
    }

    @Test
    public void deleteDeployTest() {
        List<String> list = Arrays.asList(
                "74d301db-9239-11ed-86bb-b42e99ea65c9"
        );
        for (String s : list) {
            // 删除部署
            repositoryService.deleteDeployment(s, true);
        }
    }

}
